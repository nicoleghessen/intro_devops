# Introduction to DevOps 

 AWS launch first data centre in 2004 to public 
IAAS: infrastructure a service in 2004
Outsource:
- Security
- Cleaning
- Maintaining 
- Upgrading
—> all the physical ops to a big provider 

So from <u>2010</u> the development cycle became:
- Plan 
- Dev and Test
- Deliver
- Dev and Test 
- Plan and collect feedback

Do this in a loop in order to create a quicker and easier solution for the customer. Doesn’t take years anymore to create a solution.

**Speed is everything in tech! Things are always changing**

Dev team is working fast but Ops and release is long so it then starts piling up.

2010-2015:
- Good data centres
- See Agile everywhere
- DevOps starts to come into play
- We needed to create automation so things can be deployed quicker and safer.
- DevOps is somewhat an extension of Agile and Scrum where agile and scrum is for the development team to produce features and apps in short sprints.
- Also for DevOps we want to automate and deploy the sprints right away.

<u>Rise in Agile: set of 4 principles</u>
- Individuals and interactions over processes and tools
- Working software over comprehensive documentation
- Customer collaboration over contract negotiation 
- Responding to change over following a plan


## <u>**Definition of DevOps**</u>
DevOps is a "movement" or philosophy which was created to form a better and more efficent way of working. It allowed companies to have a quicker yet more effective solution which results in more money for the company.

We do this by:
- Pipelines of code- ci/cd
- Autoscaling 
- Standardised environments with VM or containers 
- Infrastructure as code 
- Infrastructure as a service 
- Monitoring and self-healing
- Multi region to ensure high valiablity
- Continuous testing 
- Restore plans





-------------------------------------------------------------------------------

#### <u> **Link to video- Google Data Centres**</u>
[Google Data Centre](https://www.youtube.com/watch?v=XZmGGAbHqa0)
